2005-07-28 18:30 +0000 [r439708]  goutte

	* branches/KDE/3.4/kdesdk/poxml/transxx.cpp: Fix msgid starting or
	  ending with \n (Backport of #439693)

2005-09-08 16:12 +0000 [r458597]  jens

	* branches/KDE/3.4/kdesdk/kbabel/common/catalog.cpp: backport of
	  bugfix for #110981

2005-10-02 18:50 +0000 [r466471]  coolo

	* branches/KDE/3.4/kdesdk/umbrello/umbrello/dialogs/codevieweroptionsbase.ui:
	  compile with qt 3.3.5

2005-10-05 13:48 +0000 [r467523]  coolo

	* branches/KDE/3.4/kdesdk/kdesdk.lsm: 3.4.3

