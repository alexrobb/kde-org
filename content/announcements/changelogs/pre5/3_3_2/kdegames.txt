Dir: kdegames
----------------------------
new version numbers



Dir: kdegames/atlantik
----------------------------
bugfix: show correct amount of players in trade widget



Dir: kdegames/atlantik/client
----------------------------
New-patches: rob at unixcode.org--2004/atlantik--mainline--0.8--patch-39..40
Summary: kio_atlantik fixes

Backport of invitation fixes.
----------------------------
fix bug #69043



Dir: kdegames/atlantik/kio_atlantik
----------------------------
New-patches: rob at unixcode.org--2004/atlantik--mainline--0.8--patch-39..40
Summary: kio_atlantik fixes

Backport of invitation fixes.



Dir: kdegames/atlantik/libatlantic
----------------------------
bugfix: show correct amount of players in trade widget



Dir: kdegames/atlantik/libatlantikui
----------------------------
bugfix: show correct amount of players in trade widget



Dir: kdegames/debian
----------------------------
kdegames 3.3.1 packaging



Dir: kdegames/kgoldrunner/gamedata
----------------------------
backport:
fix #82584 and #83512



Dir: kdegames/kgoldrunner/src
----------------------------
Backport use isEmpty()
----------------------------
backport:
fix #82584 and #83512



Dir: kdegames/kmines/solver
----------------------------
Backport string cache



Dir: kdegames/kpat
----------------------------
Backport fix for bug 89755: Aces up move counter doesn't work

Also bump version number to 2.2.1 for error reporting purposes
----------------------------
Backport the fix for bug 89276.



Dir: kdegames/kpoker
----------------------------
Backport fixes for bugs 93635 and 93636.

BUG: 93635
BUG: 93636
