------------------------------------------------------------------------
r1043157 | djarvie | 2009-10-31 22:10:05 +0000 (Sat, 31 Oct 2009) | 2 lines

Bug 211696: If dual screens, show alarm in other screen if active window is full screen

------------------------------------------------------------------------
r1043445 | djarvie | 2009-11-01 15:19:47 +0000 (Sun, 01 Nov 2009) | 2 lines

Prevent infinite loop if NEXTRECUR time in alarm is before alarm start time

------------------------------------------------------------------------
r1043558 | winterz | 2009-11-01 20:32:04 +0000 (Sun, 01 Nov 2009) | 8 lines

Backport r1043556 by winterz from trunk to the 4.3 branch:

merge  SVN commit r1043549 by tilladam from akonadi-ports:

Avoid moving the buttons as they are being clicked by keeping them pushed to the sides at all times. 
MERGE: 4.3


------------------------------------------------------------------------
r1043972 | tmcguire | 2009-11-02 19:21:46 +0000 (Mon, 02 Nov 2009) | 10 lines

Backport r1043961 by tmcguire from trunk to the 4.3 branch:

Possibly fix a crash when exiting KMail, because the scheduler was never told to
disconnect the slave.

Bug figured out by honda@math.sci.hokudai.ac.jp, thanks very much!

BUG: 199375


------------------------------------------------------------------------
r1044030 | mkoller | 2009-11-02 21:29:22 +0000 (Mon, 02 Nov 2009) | 7 lines

Backport r1044028 by mkoller from trunk to the 4.3 branch:

CCBUG: 212304

really keep the backgroundcolor setting of a tag


------------------------------------------------------------------------
r1044036 | mkoller | 2009-11-02 21:40:41 +0000 (Mon, 02 Nov 2009) | 2 lines

revert last change as backgroundcolor of a tag is only a trunk feature

------------------------------------------------------------------------
r1044040 | mkoller | 2009-11-02 21:57:02 +0000 (Mon, 02 Nov 2009) | 7 lines

Backport r1044039 by mkoller from trunk to the 4.3 branch:

CCBUG: 212087

mail source shall always be shown left-to-right


------------------------------------------------------------------------
r1044891 | winterz | 2009-11-04 21:24:08 +0000 (Wed, 04 Nov 2009) | 8 lines

Backport r1043556 by winterz from trunk to the 4.3 branch:

merge  SVN commit r1043549 by tilladam from akonadi-ports:

Avoid moving the buttons as they are being clicked by keeping them pushed to the sides at all times. 
MERGE: 4.3


------------------------------------------------------------------------
r1045693 | winterz | 2009-11-06 16:20:21 +0000 (Fri, 06 Nov 2009) | 9 lines

Backport r1045672 by winterz from trunk to the 4.3 branch:

Apply Reinhold's fix to allow the "Pages" tab in the print dialog.
so now we have n-up options and stuff.
CCBUG: 184625
MERGE: 4.3



------------------------------------------------------------------------
r1045701 | winterz | 2009-11-06 16:46:59 +0000 (Fri, 06 Nov 2009) | 9 lines

Backport r1044694 by tmcguire from trunk to the 4.3 branch:

Don't set the due and start date when the checkboxes are not checked.
This fixes a regression that made it impossible to have todos without start
and due date.

This is a followup on r1036728 in KCal.


------------------------------------------------------------------------
r1045765 | tnyblom | 2009-11-06 18:34:04 +0000 (Fri, 06 Nov 2009) | 16 lines

Backport r1041880,1042416 by tnyblom from trunk to the 4.3 branch:

Change parseMailtoUrl() to return a QMap containing all fields.
This way we can support arbitrary fields in mailto urls.
This support needs further work as this commit only fixes this function, the use of this new functionality is left for later.

CCBUG: 122684

------------------------------------------------------------------------
r1042416 | tnyblom | 2009-10-29 20:09:26 +0100 (tor, 29 okt 2009) | 4 lines

Add ability to enterpret "In-Reply-To" header in mailto urls.

CCBUG: 122684


------------------------------------------------------------------------
r1045935 | tmcguire | 2009-11-06 23:24:30 +0000 (Fri, 06 Nov 2009) | 14 lines

Backport r1045897 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1041245 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

........
  r1041245 | tmcguire | 2009-10-27 17:56:29 +0100 (Tue, 27 Oct 2009) | 3 lines
  
  Fix logic to set the email address to the result even if the name is empty.
  Part of kolab/issue1499
........


------------------------------------------------------------------------
r1045936 | tmcguire | 2009-11-06 23:25:14 +0000 (Fri, 06 Nov 2009) | 16 lines

Backport r1045899 by tmcguire from trunk to the 4.3 branch:

SVN_MERGE
Merged revisions 1041248 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

........
  r1041248 | tmcguire | 2009-10-27 18:02:36 +0100 (Tue, 27 Oct 2009) | 5 lines
  
  Fix porting bug: Setting this to unchecked will add a checkbox to the item, we want to
  disable it.
  
  kolab/issue1499
........


------------------------------------------------------------------------
r1046106 | winterz | 2009-11-07 14:50:28 +0000 (Sat, 07 Nov 2009) | 8 lines

Backport r1046105 by winterz from trunk to the 4.3 branch:

add a crash guard for mReader when reply-all on html emails
thanks for the patch Daniel.
CCBUG: 213460
MERGE: 4.3,e4


------------------------------------------------------------------------
r1049671 | djarvie | 2009-11-15 17:09:44 +0000 (Sun, 15 Nov 2009) | 2 lines

Make it work properly with full screen Xinerama windows

------------------------------------------------------------------------
r1050136 | djarvie | 2009-11-16 18:31:34 +0000 (Mon, 16 Nov 2009) | 1 line

Fix disconnect() of non-existent timer
------------------------------------------------------------------------
r1050138 | djarvie | 2009-11-16 18:34:13 +0000 (Mon, 16 Nov 2009) | 2 lines

Bug 213019: fix for possible crash on exit.

------------------------------------------------------------------------
r1050233 | djarvie | 2009-11-16 22:25:39 +0000 (Mon, 16 Nov 2009) | 3 lines

Bug 213461: Fix crash if editing alarm from alarm window Edit button, and window changes
from reminder to normal, or from at-login to final at-login trigger time.

------------------------------------------------------------------------
r1050335 | djarvie | 2009-11-17 09:23:13 +0000 (Tue, 17 Nov 2009) | 1 line

Resize window when cancelling reminder
------------------------------------------------------------------------
r1051106 | krake | 2009-11-18 21:32:41 +0000 (Wed, 18 Nov 2009) | 4 lines

Backport of Revision 1050790

We now get all the capabilities as upper case

------------------------------------------------------------------------
r1051193 | djarvie | 2009-11-18 23:50:37 +0000 (Wed, 18 Nov 2009) | 1 line

Reschedule alarm after redisplay
------------------------------------------------------------------------
r1052048 | tmcguire | 2009-11-20 16:22:05 +0000 (Fri, 20 Nov 2009) | 2 lines

SVN_SILENT update the version to 1.12.4 for KDE 4.3.4

------------------------------------------------------------------------
r1052909 | djarvie | 2009-11-22 19:30:55 +0000 (Sun, 22 Nov 2009) | 2 lines

Bug 213461: fix crash if alarm edit dialogue is open when alarm window auto-closes.

------------------------------------------------------------------------
r1053038 | djarvie | 2009-11-23 01:04:23 +0000 (Mon, 23 Nov 2009) | 1 line

Add explanatory comment
------------------------------------------------------------------------
r1053296 | djarvie | 2009-11-23 20:11:12 +0000 (Mon, 23 Nov 2009) | 2 lines

Don't save reminder/late-cancel/show-in-KOrganizer when saving repeat-at-login alarms

------------------------------------------------------------------------
r1053344 | djarvie | 2009-11-23 21:48:00 +0000 (Mon, 23 Nov 2009) | 3 lines

Fix error saving the alarm after editing a repeat-at-login alarm.
Fix alarm dump not working.

------------------------------------------------------------------------
r1053641 | winterz | 2009-11-24 13:11:12 +0000 (Tue, 24 Nov 2009) | 2 lines

increase version number for the 4.3.4 release which will be tagged on 26 Nov.

------------------------------------------------------------------------
