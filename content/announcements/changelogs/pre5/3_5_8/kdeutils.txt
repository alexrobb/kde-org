2007-05-27 18:46 +0000 [r668809]  hasso

	* branches/KDE/3.5/kdeutils/ksim/monitors/cpu/ksimcpu.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/misc_python.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/memsensor.h,
	  branches/KDE/3.5/kdeutils/ksim/monitors/filesystem/filesystemstats.cpp,
	  branches/KDE/3.5/kdeutils/ksim/monitors/disk/ksimdisk.cpp,
	  branches/KDE/3.5/kdeutils/superkaramba/src/memsensor.cpp: Make
	  ksim and superkaramba compile and work in DragonFly BSD. All
	  patches by Joerg Sonnenberger via pkgsrc.

2007-05-28 10:18 +0000 [r669000]  mueller

	* branches/KDE/3.5/kdeutils/ark/arch.h,
	  branches/KDE/3.5/kdeutils/kdf/disklist.h,
	  branches/KDE/3.5/kdeutils/kregexpeditor/KMultiFormListBox/kmultiformlistboxentry.h,
	  branches/KDE/3.5/kdeutils/kdf/kdfwidget.h,
	  branches/KDE/3.5/kdeutils/kdf/kdfconfig.h,
	  branches/KDE/3.5/kdeutils/kregexpeditor/KWidgetStreamer/kwidgetstreamer.h,
	  branches/KDE/3.5/kdeutils/kdf/disks.h,
	  branches/KDE/3.5/kdeutils/kdelirc/kdelirc/iraction.h,
	  branches/KDE/3.5/kdeutils/kedit/ktextfiledlg.h,
	  branches/KDE/3.5/kdeutils/kregexpeditor/regexp.h,
	  branches/KDE/3.5/kdeutils/kregexpeditor/pair.h,
	  branches/KDE/3.5/kdeutils/kregexpeditor/userdefinedregexps.h: the
	  usual "daily unbreak compilation"

2007-05-29 12:33 +0000 [r669447]  mueller

	* branches/KDE/3.5/kdeutils/superkaramba/src/textlabel.h: this is
	  not a typedef

2007-06-01 11:44 +0000 [r670409]  lunakl

	* branches/KDE/3.5/kdeutils/kfloppy/format.cpp: Check all available
	  densities instead of a hardcoded list. This fixes autodetect.

2007-06-01 11:52 +0000 [r670413]  lunakl

	* branches/KDE/3.5/kdeutils/kfloppy/floppy.cpp: Default to density
	  autodetect. CCBUG: 94392

2007-06-06 19:51 +0000 [r672332]  mlaurent

	* branches/KDE/3.5/kdeutils/kwallet/main.cpp: Fix mem leak

2007-06-21 12:27 +0000 [r678445]  binner

	* branches/KDE/3.5/kdeutils/ark/ark.desktop,
	  branches/KDE/3.5/kdeutils/kedit/KEdit.desktop,
	  branches/KDE/3.5/kdeutils/kgpg/kgpg.desktop,
	  branches/KDE/3.5/kdeutils/kwallet/kwalletmanager.desktop,
	  branches/KDE/3.5/kdeutils/superkaramba/src/superkaramba.desktop,
	  branches/KDE/3.5/kdeutils/kdf/kwikdisk.desktop,
	  branches/KDE/3.5/kdeutils/kdelirc/irkick/irkick.desktop: fix
	  invalid .desktop files

2007-08-15 20:52 +0000 [r700562]  bero

	* branches/KDE/3.5/kdeutils/klaptopdaemon/Makefile.am: Fix kded
	  [kdeinit] --new-startup: symbol lookup error:
	  /usr/lib/kde3/kded_klaptopdaemon.so: undefined symbol:
	  XScreenSaverQueryExtension when building with XScrnSaver support

2007-09-08 00:47 +0000 [r709651]  mlaurent

	* branches/KDE/3.5/kdeutils/kgpg/listkeys.cpp: change status bar
	  message when we can import it

2007-09-11 20:53 +0000 [r711248]  mueller

	* branches/KDE/3.5/kdeutils/klaptopdaemon/xautolock.cc: stop
	  filling gigabytes of log file

2007-09-21 10:21 +0000 [r715146]  binner

	* branches/KDE/3.5/kdeutils/ark/ark.desktop: application/zip is
	  both IANA and freedesktop mime-type

2007-10-04 22:53 +0000 [r721296]  dfaure

	* branches/KDE/3.5/kdeutils/klaptopdaemon/daemondock.cpp: Apply
	  patch from 143859, fixing lock & hibernate. "The problem is that
	  the dcop call used for locking the system (send) is asynchronous
	  so the hibernation starts without waiting for the locking being
	  already done" BUG: 143859

2007-10-08 11:06 +0000 [r722973]  coolo

	* branches/KDE/3.5/kdeutils/kdeutils.lsm: updating lsm

