2008-09-26 22:05 +0000 [r865177]  shaforo <shaforo@localhost>:

	* branches/KDE/4.1/kdesdk/lokalize/src/catalog/gettext/catalogitem.cpp,
	  branches/KDE/4.1/kdesdk/lokalize/src/catalog/gettext/catalogitem.h:
	  fix

2008-09-27 15:59 +0000 [r865449]  lueck <lueck@localhost>:

	* branches/KDE/4.1/kdesdk/doc/kate/mdi.docbook,
	  branches/KDE/4.1/kdesdk/doc/kate/configuring.docbook,
	  branches/KDE/4.1/kdesdk/doc/lokalize/index.docbook,
	  branches/KDE/4.1/kdesdk/doc/kate/fundamentals.docbook,
	  branches/KDE/4.1/kdesdk/doc/lokalize/original-diff.png,
	  branches/KDE/4.1/kdesdk/doc/kate/advanced.docbook,
	  branches/KDE/4.1/kdesdk/doc/kate/highlighting.docbook,
	  branches/KDE/4.1/kdesdk/doc/kate/part.docbook,
	  branches/KDE/4.1/kdesdk/doc/kate/index.docbook,
	  branches/KDE/4.1/kdesdk/doc/kate/menus.docbook,
	  branches/KDE/4.1/kdesdk/doc/cervisia/index.docbook: documentation
	  backport for 4.1.3 from trunk

2008-09-29 20:20 +0000 [r866029]  lueck <lueck@localhost>:

	* branches/KDE/4.1/kdesdk/doc/lokalize/glossary.png (added): add
	  forgotten screenshot for backport

2008-10-02 06:18 +0000 [r866867]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdesdk/kate/plugins/filetemplates/plugin/katefiletemplates.desktop,
	  branches/KDE/4.1/kdesdk/kioslave/svn/svnhelper/subversion.desktop:
	  SVN_SILENT made messages (.desktop file)

2008-10-06 06:16 +0000 [r868355]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdesdk/kate/plugins/externaltools/kateexternaltoolsplugin.desktop,
	  branches/KDE/4.1/kdesdk/kate/plugins/skeleton/kateexternaltoolsplugin.desktop:
	  SVN_SILENT made messages (.desktop file)

2008-10-08 21:36 +0000 [r869396]  ogoffart <ogoffart@localhost>:

	* branches/KDE/4.1/kdesdk/kcachegrind/kcachegrind/callgraphview.cpp:
	  ckport crash fix

2008-10-09 18:11 +0000 [r869673]  shaforo <shaforo@localhost>:

	* branches/KDE/4.1/kdesdk/lokalize/src/kaiderview.cpp: fix
	  'Lokalize inserting invisible paragraph separator'

2008-10-11 07:59 +0000 [r869821]  shaforo <shaforo@localhost>:

	* branches/KDE/4.1/kdesdk/lokalize/src/kaiderview.cpp: fix bug

2008-10-12 06:11 +0000 [r870248]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdesdk/kate/plugins/xmltools/katexmltools.desktop:
	  SVN_SILENT made messages (.desktop file)

2008-10-17 06:07 +0000 [r872393]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdesdk/kapptemplate/templates/C++/kpartapp/src/%{APPNAMELC}_part.desktop,
	  branches/KDE/4.1/kdesdk/kate/plugins/tabbarextension/katetabbarextension.desktop,
	  branches/KDE/4.1/kdesdk/kate/plugins/kpybrowser/katepybrowse.desktop,
	  branches/KDE/4.1/kdesdk/cervisia/cvsservice/cvsservice.desktop,
	  branches/KDE/4.1/kdesdk/kate/plugins/textfilter/katetextfilter.desktop,
	  branches/KDE/4.1/kdesdk/kate/plugins/openheader/kateopenheader.desktop,
	  branches/KDE/4.1/kdesdk/kate/plugins/externaltools/kateexternaltoolsplugin.desktop,
	  branches/KDE/4.1/kdesdk/kate/plugins/filebrowser/katefilebrowserplugin.desktop,
	  branches/KDE/4.1/kdesdk/kate/data/kateplugin.desktop,
	  branches/KDE/4.1/kdesdk/kate/plugins/helloworld/katehelloworld.desktop,
	  branches/KDE/4.1/kdesdk/kapptemplate/templates/C++/kapp4/src/%{APPNAMELC}.desktop,
	  branches/KDE/4.1/kdesdk/kate/plugins/mailfiles/katemailfilesplugin.desktop,
	  branches/KDE/4.1/kdesdk/kapptemplate/templates/C++/kpartapp/src/%{APPNAMELC}.desktop,
	  branches/KDE/4.1/kdesdk/kate/plugins/make/katemake.desktop,
	  branches/KDE/4.1/kdesdk/kate/plugins/filetemplates/plugin/katefiletemplates.desktop,
	  branches/KDE/4.1/kdesdk/kate/plugins/xmlcheck/katexmlcheck.desktop,
	  branches/KDE/4.1/kdesdk/kate/plugins/xmltools/katexmltools.desktop,
	  branches/KDE/4.1/kdesdk/kate/plugins/findinfiles/katefindinfilesplugin.desktop,
	  branches/KDE/4.1/kdesdk/kate/plugins/konsole/katekonsoleplugin.desktop,
	  branches/KDE/4.1/kdesdk/kapptemplate/kapptemplate.desktop,
	  branches/KDE/4.1/kdesdk/kate/plugins/skeleton/kateexternaltoolsplugin.desktop,
	  branches/KDE/4.1/kdesdk/kate/plugins/insertcommand/kateinsertcommand.desktop,
	  branches/KDE/4.1/kdesdk/kate/data/kate.desktop,
	  branches/KDE/4.1/kdesdk/kate/plugins/quickdocumentswitcher/katequickdocumentswitcher.desktop,
	  branches/KDE/4.1/kdesdk/kate/plugins/snippets/katesnippets.desktop,
	  branches/KDE/4.1/kdesdk/kate/plugins/htmltools/katehtmltools.desktop,
	  branches/KDE/4.1/kdesdk/kate/plugins/symbolviewer/katesymbolviewer.desktop:
	  SVN_SILENT made messages (.desktop file)

2008-10-22 09:50 +0000 [r874686]  ilic <ilic@localhost>:

	* branches/KDE/4.1/kdesdk/scripts/extractattr: Switch to file:line
	  format in extracted comment. (bport: 874684)

2008-10-22 15:57 +0000 [r874864]  rdieter <rdieter@localhost>:

	* branches/KDE/4.1/kdesdk/poxml/CMakeLists.txt: reduce extraneous
	  linkage (libkdecore)

2008-10-25 16:07 +0000 [r875790]  woebbe <woebbe@localhost>:

	* branches/KDE/4.1/kdesdk/poxml/CMakeLists.txt: gold also needs Qt
	  Core

2008-10-28 17:27 +0000 [r877105]  woebbe <woebbe@localhost>:

	* branches/KDE/4.1/kdesdk/cervisia/version.h: bump version number

