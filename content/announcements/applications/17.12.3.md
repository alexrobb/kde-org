---
aliases:
- ../announce-applications-17.12.3
date: 2018-03-08
description: KDE Ships KDE Applications 17.12.3
changelog: true
layout: application
title: KDE Ships KDE Applications 17.12.3
version: 17.12.3
---

{{% i18n_var "March 8, 2018. Today KDE released the third stability update for <a href='%[1]s'>KDE Applications 17.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../17.12.0" %}}

About 25 recorded bugfixes include improvements to Kontact, Dolphin, Gwenview, JuK, KGet, Okular, Umbrello, among others.

Improvements include:

- Akregator no longer erases the feeds.opml feed list after an error
- Gwenview's fullscreen mode now operates on the correct filename after renaming
- Several rare crashes in Okular have been identified and fixed
