---
aliases:
- ../announce-applications-17.08.3
changelog: true
date: 2017-11-09
description: KDE Ships KDE Applications 17.08.3
layout: application
title: KDE Ships KDE Applications 17.08.3
version: 17.08.3
---

{{% i18n_var "November 9, 2017. Today KDE released the third stability update for <a href='%[1]s'>KDE Applications 17.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../17.08.0" %}}

About a dozen recorded bugfixes include improvements to Kontact, Ark, Gwenview, KGpg, KWave, Okular, Spectacle, among others.

{{% i18n_var "This release also includes the last version of KDE Development Platform %[1]s." "4.14.38" %}}

Improvements include:

- Work around a Samba 4.7 regression with password-protected SMB shares
- Okular no longer crashes after certain rotation jobs
- Ark preserves file modification dates when extracting ZIP archives