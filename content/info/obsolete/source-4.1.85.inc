<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">MD5&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.85/src/kdeaccessibility-4.1.85.tar.bz2">kdeaccessibility-4.1.85</a></td><td align="right">6.4MB</td><td><tt>9d99981713d0e36bf85dc70081562a25</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.85/src/kdeadmin-4.1.85.tar.bz2">kdeadmin-4.1.85</a></td><td align="right">1.9MB</td><td><tt>612a2b1257186ec4aa86137690be8a71</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.85/src/kdeartwork-4.1.85.tar.bz2">kdeartwork-4.1.85</a></td><td align="right">41MB</td><td><tt>bd9a81e95cf9b6d7aff427fef343f5f1</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.85/src/kdebase-4.1.85.tar.bz2">kdebase-4.1.85</a></td><td align="right">4.1MB</td><td><tt>891466362da4d839c15e824d1008f043</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.85/src/kdebase-runtime-4.1.85.tar.bz2">kdebase-runtime-4.1.85</a></td><td align="right">66MB</td><td><tt>100cdcc07031bf04d12f7e23f9bc5945</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.85/src/kdebase-workspace-4.1.85.tar.bz2">kdebase-workspace-4.1.85</a></td><td align="right">47MB</td><td><tt>2efd6fbcbebea61bd63f66e6a7ab6117</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.85/src/kdebindings-4.1.85.tar.bz2">kdebindings-4.1.85</a></td><td align="right">4.7MB</td><td><tt>31b04d423ebbce5faf7eaa2f92409083</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.85/src/kdeedu-4.1.85.tar.bz2">kdeedu-4.1.85</a></td><td align="right">57MB</td><td><tt>2e29ee1e501d46bd00c67c0b5bfdebd4</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.85/src/kdegames-4.1.85.tar.bz2">kdegames-4.1.85</a></td><td align="right">37MB</td><td><tt>eb54c3b627f382fa694fa0ab8a40b3bb</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.85/src/kdegraphics-4.1.85.tar.bz2">kdegraphics-4.1.85</a></td><td align="right">3.4MB</td><td><tt>8a73aa0b31d58b9eacb69b465c779a52</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.85/src/kdelibs-4.1.85.tar.bz2">kdelibs-4.1.85</a></td><td align="right">11MB</td><td><tt>20aa488389768aea440d211b66437f5b</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.85/src/kdemultimedia-4.1.85.tar.bz2">kdemultimedia-4.1.85</a></td><td align="right">1.5MB</td><td><tt>a2339f800000e2c3cc687a8535da6ed7</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.85/src/kdenetwork-4.1.85.tar.bz2">kdenetwork-4.1.85</a></td><td align="right">7.2MB</td><td><tt>609e06dce93a061addce565dc3820540</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.85/src/kdepim-4.1.85.tar.bz2">kdepim-4.1.85</a></td><td align="right">13MB</td><td><tt>73ab9aef5017936e6e0d6f17506ab32d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.85/src/kdepimlibs-4.1.85.tar.bz2">kdepimlibs-4.1.85</a></td><td align="right">1.6MB</td><td><tt>fd82b2b05cece9d02ae3228bc715b0e3</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.85/src/kdeplasma-addons-4.1.85.tar.bz2">kdeplasma-addons-4.1.85</a></td><td align="right">4.6MB</td><td><tt>c961f6b0391646224aaf21b9aa63af04</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.85/src/kdesdk-4.1.85.tar.bz2">kdesdk-4.1.85</a></td><td align="right">5.2MB</td><td><tt>9fff92efdd5ca6f8b60f293a6830fe70</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.85/src/kdetoys-4.1.85.tar.bz2">kdetoys-4.1.85</a></td><td align="right">1.3MB</td><td><tt>63380dbff8492b5183230bb3a2634d80</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.85/src/kdeutils-4.1.85.tar.bz2">kdeutils-4.1.85</a></td><td align="right">2.2MB</td><td><tt>c5767898344b613a98f66664c1507d77</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.1.85/src/kdewebdev-4.1.85.tar.bz2">kdewebdev-4.1.85</a></td><td align="right">3.3MB</td><td><tt>f45489e61335fc304388db9e4af4a6a6</tt></td></tr>
</table>
