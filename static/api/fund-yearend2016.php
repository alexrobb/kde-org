<?php
    require("www_config.php");
    $datas = [];

    $countStmt = $dbConnection->prepare("SELECT COUNT(*) FROM yearend2016donations ORDER BY CREATED_AT DESC;");
    $countStmt->execute();
    $n = $countStmt->fetchColumn();

    $datas['n'] = $n;
    $datas['donations'] = [];
    $datas['top'] = [];

    $res = $dbConnection->query("SELECT *, UNIX_TIMESTAMP(date) AS date_t FROM yearend2016donations ORDER BY date DESC;");

    $total = 0;
    $index = $n;
    while ($row = $res->fetch()) {
        $msg = htmlspecialchars($row["message"]);
        if ($msg == "") {
            $msg = i18n_var("Anonymous donation");
        }
        $data = [
            'msg' => $msg,
            'amount' => number_format($row["amount"], 2),
            'index' => $index,
            'date' => .date("jS F Y", $row["date_t"]),
        ];
        $datas['donations'][] = $data; 
        $index--;
    }
    $datas['total'] = number_format($total, 0, ".", "&#8201;");

    // This is select from select seems mysql specific?
    $query = "select MIN(sum_amount) from ( select sum(amount) as sum_amount from yearend2016donations group by email order by sum_amount DESC LIMIT 9 ) as tops;";
    $limitStmt = $dbConnection->prepare($query);
    $limitStmt->execute();
    $limit_amount = $limitStmt->fetchColumn();

    $query = "select * from ( select email, sum(amount) as sum_amount from yearend2016donations group by email order by sum_amount ) as tops where sum_amount >= :limit_amount order by sum_amount DESC;";
    $q = $dbConnection->prepare($query);
    $q->execute(['limit_amount' => $limit_amount]);

    $count = 0;
    $last_amount = -1;
    $last_was_same_amount = false;
    $q2 = $dbConnection->prepare('select distinct message as name from yearend2016donations where email=:email');
    while ($row = $q->fetch()) {
        $amount = $row["sum_amount"];
        $email = $row['email'];
        $q2->execute(['email' => $email]);
        $donation_count = 0;
        while ($row2 = $q2->fetch()) {
            $msg = $row2["name"];
            $donation_count++;
        }

        if ($donation_count !== 1) {
            $msg = "Anonymous donation";
        }
        if ($msg == "") {
            $msg = "Anonymous donation";
        }

        if ($amount == $last_amount) {
            // Do not touch index
        } else {
            // Set index to correct value
            $index = $count + 1;
        }
        $data = [
            'msg' => $msg,
            'amount' => number_format($amount, 2),
            'index' => $index,
        ];
        $count++;
        $datas['donations'][] = $data; 
    }
    echo json_encode($datas);
?>
